# puppeteer-realmouse

Realistic mouse visualization and movement for Puppeteer. 

## Warning

This library is only supported with Puppeteer `13.5.1` as this was the latest version I used when implementing the library. Puppeteer seem to have change in certain areas since that break functionality of this library as, for example, it cannot resolve the `objectId` of elements. Unfortunately, I do not have time to catch up on the changes and figure things out.

## Prerequisites

In order to use this plugin:

<ul>
    <li>Puppeteer must be installed.</li>
    <li>Pupepteer's page object must be created.</li>
</ul>

## Installation

To install the plugin to your project please use:

```javascript
npm install --save puppeteer-realmouse
```

## Manual

Once Puppeteer video recorder is installed, you can require it in your project:

```javascript
import { Mouse } from 'puppeteer-realmouse';

// Assuming Puppeteer's page object is `page`.
const mouse = new Mouse(page);
await mouse.install();
await page.goto('https://www.google.com');

const acceptButton = await mouse.moveTo('button[id=L2AGLb]', {
    waitAfter: 1000
});
await acceptButton.click();
const input = await mouse.moveTo('input[name=q]', {
    waitBefore: 1000,
    waitAfter: 1000
});

```

The `moveTo` method's first argument is a `selector`. The following selectors are supported:

  - (dict): `x` and `y` coordinates, e.g.: `{ x: 100, y: 200 }`
  - (handle): HTML element handle
  - (string): Selector, e.g.: `button[id=L2AGLb]`
  - (string): XPath selector, e.g.: `/html`

The `moveTo` method's second argument an optional dictionary of `options`. The following options are supported:

  - immediate (optional): Set to `true` to move the mouse immediately. Realistic movement is not applied.
  - waitForSelector (optional): A number in milliseconds to wait for the item matching the `selector` to show up.
  - waitBefore (optional): A number representing how long to wait in milliseconds before starting to move the mouse.
  - waitAfter (optional): A number representing how long to wait in milliseconds after the mouse arrived to the destination.

The `moveTo` method returns:

   - The handle of the HTML element the mouse was moved to in case `selector` was a string and the element existed. **OR,**
   - The coordinates as a dictionary if the `selector` was a dictionary of coordinates.

## FAQ

### Does it support Chrome in headless mode?

Yes, it supports Chrome in both headless and headful mode.

<br/>

### Does it use the window object?

No, it doesn't use the window object.

<br/>

### Can I run this plugin with my own page/browser objects?

Yes.

<br/>

### Will it change my browser/page/window objects?

It injects some HTML elements and stylesheet into the page for mouse visualization.

<br/>
