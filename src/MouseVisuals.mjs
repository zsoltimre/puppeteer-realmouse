export function visualHandler() {
    const mouseCursor = document.createElement('puppeteer-mouse-pointer');
    const styleElement = document.createElement('style');
    // Cursor images from: https://tobiasahlin.com/blog/common-mac-os-x-lion-cursors/
    // Awesome converter for the lazies: https://ezgif.com/image-to-datauri
    styleElement.innerHTML = `
        puppeteer-mouse-pointer {
            pointer-events: none;
            position: absolute;
            top: 0;
            z-index: 2147483646;
            left: 0;
            width: 14px;
            height: 21px;
            background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA4AAAAVCAMAAABBhy+7AAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAA+VBMVEUAAAAAAAD7+/sAAAD09PQAAAAAAAD09PTz8/PS0tJqampubm5+fn6np6cAAAD19fXw8PD39/cJCQkAAAAAAAAAAAAAAAD19fWzs7OgoKDX19cAAAD+/v6ysrIAAAAICAj29vb09PQAAAAAAADFxcUAAAAAAADR0dH8/Py2trYAAAAAAAAAAABRUVH29vb39/fJycni4uLp6emjo6MHBwcAAAAAAAAAAAD////T09MUFBTS0tIAAAAXFxfb29sbGxseHh4aGhrr6+vv7+/39/c5OTni4uIKCgqurq5BQUFmZmZDQ0PQ0NDW1tY9PT1tbW2np6cgICCioqIFyGmYAAAAOHRSTlMAAooIjg4QiH6dTUpBMQT92N4bFBILA/t4aU0B+3AgIt6zBQ1lGwqE/CMGCQcp9txCpsJpJQ8iGe73bXgAAAABYktHRACIBR1IAAAACXBIWXMAAAsSAAALEgHS3X78AAAArUlEQVQY003P1xKCMBRFUaKCvSuWWFFsYG9Rr1jA3v3/jzGMwOS8rac9h0MuxDFzEw9rnixYC2S5YiwQYE0Ja82xSdhoXsSQ2ocYOrZo26Zlk1vY7XXdINSUh+MJzv5AMBSOUF6isSvc4olkKi1SZrK5O8AjLyKEOaFQLJUrT3hVJTNUq8sNofmGT6uNKTu8gtVu72v0B2YXi5hDqeFoPJk6txR5NuclbBOpyh8/ou0enbQh1QcAAAAASUVORK5CYII=);
            transition: none;
        }
        puppeteer-mouse-pointer.hover-link {
            width: 18px;
            height: 19px;
            background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAATCAMAAACqTK3AAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAABiVBMVEUAAAAAAAAdHR0YGBgZGRkAAAAAAAAoKCgjIyMAAAAAAAAwMDCXl5cVFRUAAAAZGRkdHR0AAAAAAAAAAAAeHh4LCwssLCwQEBAYGBgYGBgSEhIAAAAUFBQxMTECAgILCwsYGBgcHBwbGxsXFxcYGBgCAgJXV1cfHx82NjYAAAAAAAAXFxckJCQ/Pz8AAAAAAAAWFhYTExM/Pz8AAAAAAAAAAAAhISEAAAAAAAANDQ1RUVEXFxcAAAAYGBiQkJAXFxcAAAAAAAAaGhoWFhYAAAAAAAAAAAAAAAAFBQUfHx9BQUEDAwMAAAAAAAAGBgYkJCRBQUEtLS0dHR0AAAAAAAAREREtLS0ZGRkWFhYGBgYAAAAAAAAAAAAAAAAAAAAAAAAAAAD5+fn////a2trf39/9/f2fn5/Ly8vz8/Pb29tpaWnMzMw4ODjr6+v19fXV1dX6+vrOzs7R0dH39/fy8vLv7+83Nzc1NTU2NjYuLi4vLy/4+Pi5ubni4uLT09Pc3Nzx8fH+/v4AAABAFFICAAAAYXRSTlMAA6zsewIa8PIlAe7+ihHw0B8dD9n48K/n6EgMmvv+8eq8ce3hg/P97iQH9PLvLw3g/fAxC0vzBReD9u4I1f2nDkLy6y0JBBVj9PNaEhle9PX18AYb6/O2ofkTNEkmPjAQTIDc2AAAAAFiS0dEAIgFHUgAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAENSURBVBjTY2BgYGBkYmZhZUABbOyJHJxcyCJcnNxJPLyMKEJ8/EnJAoJCyOqERVJSRTnExCUkpWDC0jJpSbLpGXLJmfIKikADWJWUVVTVkrKyk3KSktRzNTS5GLSY8/K1dZIgoCBJV4+LQd+gMKnIMCmpOKkkKSMpyciYlcHE1Kw0KTcpqSypPKkiKUlHiJXB3MLSCqSnMqkKCKut+bgYuGyEbGuAQlVgaGfvAHQCq4OjU20SUBsQ1jm7uIKc5ebu4VkPNLw0qcHL2wfsMS4tXz//pKTGgMDEILZgqCdtQkKNjMLCI/wjo2A+4tKPjolxjOWMi5eGe9xN3ycqQdokQd8cKYBYWbmADgJrAwCJ7T9xICBvoAAAAABJRU5ErkJggg==);
        }
        puppeteer-mouse-pointer.hover-input {
            width: 7px;
            height: 17px;
            background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAcAAAARCAMAAAAmA0ZnAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAYFBMVEUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD6T+iNAAAAH3RSTlMA4MFGQb05dPN79no6Zf5jApAEaf1CfPSBRd+4QDi1YsdPHAAAAAFiS0dEAIgFHUgAAAAJcEhZcwAACxIAAAsSAdLdfvwAAABHSURBVAjXY2BkYmZgYWVkYGPn4OTi5mFgYODl42cAAQF5ATDNIM+AlxaUF0ShcakTgponLCIMJEXFONjFJSQZpKRlGGTlpADEDwO3fdVBnAAAAABJRU5ErkJggg==);
        }
        puppeteer-mouse-pointer.hover-draggable {
            width: 20px;
            height: 20px;
            background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAABhGlDQ1BJQ0MgcHJvZmlsZQAAKJF9kT1Iw0AcxV9TpUUqDnYQFcxQnSyIijpqFYpQIdQKrTqYXPoFTRqSFBdHwbXg4Mdi1cHFWVcHV0EQ/ABxdHJSdJES/5cUWsR6cNyPd/ced+8AoVZimtUxBmi6bSbjMTGdWRUDr/BjCEFMY0BmljEnSQm0HV/38PH1Lsqz2p/7c3SrWYsBPpF4lhmmTbxBPLVpG5z3icOsIKvE58SjJl2Q+JHrisdvnPMuCzwzbKaS88RhYjHfwkoLs4KpEU8SR1RNp3wh7bHKeYuzVqqwxj35C0NZfWWZ6zQHEcciliBBhIIKiijBRpRWnRQLSdqPtfH3u36JXAq5imDkWEAZGmTXD/4Hv7u1chPjXlIoBnS+OM7HMBDYBepVx/k+dpz6CeB/Bq70pr9cA2Y+Sa82tcgR0LMNXFw3NWUPuNwB+p4M2ZRdyU9TyOWA9zP6pgzQewt0rXm9NfZx+gCkqKvEDXBwCIzkKXu9zbuDrb39e6bR3w+lGXK7DDxyoQAAAAZiS0dEAP8A/wD/oL2nkwAAAAlwSFlzAAALEwAACxMBAJqcGAAAAm1JREFUOMvtVDFP21AQvpA6tU2EFGI/OzwihpBMAQaLDlloJzaksFTdu8BUqXPVgW5Vu3TpXmVCIgNjpUTwAxASAokMiZAaRQpx4tR5TpQavi4YNQopmTr1W+6d7u7Td3fvPaJ/gHkiUoiIDMN4HY/HHcaYrev6SyKaIaKnUzMlk8mP6XTa0TTNliTJMgzjh23bqNfriEaj15qmNTnnDuf87VSEmqY1hsMhzs7OYBhGjXPewh0kSfIPDw/heR4YY61JHDN/OqqqVk5OTiibzZJlWfNCiEgQk2U5tL6+ToqiUDgcDnHO38Tj8fbi4uK3iQoVRXm1s7PjAcDBwQFUVR0GCufm5m593wcA6LreSaVSjhACiUSi+7euI4wxWwiBXq+HWCx2GxByzoMjTNPs7+/vAwAYY+2xlpeWlj5pmtY1TfNSCFErFos0OztL29vboSAxn8/fF+VyOXlra4uurq4oEonUx2QZhuH4vo9KpQLTNN18Pu9gChQKBSQSic9jCgH0arUapdNpKpfL0VarFQ0Szs/PiYjo9PR0xBIRHR8fi0aj8X1MoSzLL5aXl7uOMy5sbW3tQQsAKysrNhHpYwoHg0G50+m8293dFZO2NRgMRnzXdanZbP4iousH76Ft219KpVL16OhopLDf7z9IuLe3NwTw9bHHkllYWOhUq9X7tjY2NgAAuVwOALC5uYlSqQTG2CURRR59frIsP08mk87FxcXILF3XRTBjy7LaRJSZ+pOQJOnZ6urq/YaEEMhkMj9TqVTXdV1wzu1JtaFJAVVV27qu426OT8Lh8HsA/s3NzQchRMjzvBgR3dJ//AZ5o5u9pyB1zAAAAABJRU5ErkJggg==);
        }
      `;

    function updateMouseCursor(shouldSet, className) {
        if (shouldSet) {
            mouseCursor.classList.add(className);
            return;
        }
        mouseCursor.classList.remove(className);
    }

    function applyClassForClickable(element) {
        const tag = element.tagName.toLowerCase();
        updateMouseCursor(
            element.onclick || ['a', 'button'].includes(tag),
            'hover-link'
        );
    }

    function applyClassForDraggable(element) {
        updateMouseCursor(element.draggable, 'hover-draggable');
    }

    function applyClassForInput(element) {
        const tag = element.tagName.toLowerCase();
        updateMouseCursor(['input'].includes(tag), 'hover-input');
    }

    document.head.appendChild(styleElement);
    document.body.appendChild(mouseCursor);
    document.addEventListener(
        'mousemove',
        (event) => {
            applyClassForClickable(event.target);
            applyClassForInput(event.target);
            applyClassForDraggable(event.target);
            mouseCursor.style.left = event.pageX + 'px';
            mouseCursor.style.top = event.pageY + 'px';
        },
        true
    );
    document.addEventListener(
        'mousedown',
        (event) => {
            // TODO
        },
        true
    );
    document.addEventListener(
        'mouseup',
        (event) => {
            // TODO
        },
        true
    );
}
