// Things in here are taken from: https://github.com/Xetera/ghost-cursor

import { fitts, bezierCurve } from './math.mjs';

function clampPositive(vectors) {
    const clamp0 = (elem) => Math.max(0, elem);
    return vectors.map((vector) => {
        return {
            x: clamp0(vector.x),
            y: clamp0(vector.y),
        };
    });
}

export function path(start, end, spreadOverride) {
    const defaultWidth = 100;
    const minSteps = 25;
    const width = 'width' in end ? end.width : defaultWidth;
    const curve = bezierCurve(start, end, spreadOverride);
    const length = curve.length() * 0.8;
    const baseTime = Math.random() * minSteps;
    const steps = Math.ceil(
        (Math.log2(fitts(length, width) + 1) + baseTime) * 3
    );
    const re = curve.getLUT(steps);
    return clampPositive(re);
}

export async function tracePath(page, vectors) {
    let lastPosition = null;
    for (const v of vectors) {
        try {
            await page.mouse.move(v.x, v.y);
            lastPosition = v;
        } catch (error) {
            // Exit function if the browser is no longer connected
            if (!page.browser().isConnected()) return;

            console.debug(
                'Warning: could not move mouse, error message:',
                error
            );
        }
    }
    return lastPosition;
}
